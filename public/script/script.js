
//Endereços
const categoriasAPI = "http://localhost:8888/api/V1/categories/list";
const produtosAPI = "http://localhost:8888/api/V1/categories/"; // + {id}


// Importando scripts
let imports = [
    'view/avisos.js',
    'model/auxPromise.js',
    'controller/acessaAPI.js',
    'view/mostraCategorias.js',
    'controller/carregaCatsProds.js',
    'view/mostraProdutos.js',
    'view/iniciando.js'
];

imports.forEach(src => {
    let script = document.createElement('script');
    script.src = "script/"+src;
    document.head.appendChild(script);
});


