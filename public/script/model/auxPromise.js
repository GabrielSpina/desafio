class MyPromise{
    constructor(end){
        return new Promise(
            (resolve, reject) => {
                let http = new XMLHttpRequest();
    
                http.onreadystatechange = function() {
                    if(this.readyState == 4 && this.status == 200) {
                        resolve(this.responseText); 
                    } else if(this.readyState == 4 && this.status != 200){
                        reject("sem acesso a API");
                    }
                };
            
                http.open("GET", end);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                http.send();
            });
    }
}