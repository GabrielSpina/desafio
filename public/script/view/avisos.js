class Avisos{
    constructor(){
        this.msg = "";
        this.av = document.getElementById("avisos");
    }

    mostra(){
        this.av.style.marginTop = "15px";
    }

    esconde(){
        this.av.style.marginTop = "-70px";
    }

    aviso(msg){
        this.msg = msg;
        this.av.innerText= this.msg;
        this.mostra();
    }


}

let avisos = new Avisos();