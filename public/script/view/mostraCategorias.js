
//Script para mostrar a lista de categorias carregadas
class MostraCategorias{
    constructor(){

        this.listaCats = document.querySelector("#categorias ul");
    }

    mostra(lista, objCarregaProds){
        lista.forEach(cat => {
            let li = document.createElement("li");
            let text = document.createTextNode(cat.name);
            li.appendChild(text);

            li.addEventListener('click', function(){
                objCarregaProds.carregaProds(lista, cat.id);
            })

            this.listaCats.appendChild(li);


        });
    }


}