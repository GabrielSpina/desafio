class MostraProdutos{
    constructor(){
        this.caminho = document.getElementById("pagAtual");
        this.catAtual = document.getElementById("categoria");

        this.filtroCores = document.getElementById("cores");
        this.paleta = document.getElementById("paleta");

        this.filtroTipo = document.querySelector("#tipo");
        this.ul = this.filtroTipo.querySelector("ul");

        this.corpo = document.getElementById("corpo");
    }

    mostraTitulos(catAtual){
        this.caminho.textContent = "Página inicial > "+catAtual;
        this.catAtual.textContent = catAtual;
    }

    filtros(listaProds){
        //Procurando filtros
        let chaves = Object.keys(listaProds.filters[0]);
        
        //Testa quais filtros existem
        //Filtro de cor
        if(!chaves.indexOf("color")){
            this.filtroCores.style.display = "block";
            let cores = [];
            listaProds.items.forEach(cor => {
                cores.push(cor.filter[0].color);
            });
            cores = cores.filter((j, i) => cores.indexOf(j) == i);
            this.mostraPaletaCores(cores, listaProds);
        } else {
            this.filtroCores.style.display = "none";
        }
        //Filtro de genero
        if(!chaves.indexOf("gender")){
            this.filtroTipo.style.display = "block";
            this.mostraGeneros(listaProds);

        } else {
            this.filtroTipo.style.display = "none";
        }
    }

    mostraPaletaCores(cores, listaProds){
        this.paleta.innerHTML = "";
        let corTra;
        cores.forEach(cor => {
            //Traduzindo a cor;
            switch(cor){
                case "Preta":
                    corTra = "black";
                    break;
                case "Preto":
                    corTra = "black";
                    break;
                case "Laranja":
                    corTra = "orange";
                    break;
                case "Amarela":
                    corTra = 'yellow';
                    break;
                case "Rosa":
                    corTra = 'pink';
                    break;
                case "Cinza":
                    corTra = 'gray';
                    break;
                case "Azul":
                    corTra = 'blue';
                    break;
                case "Bege":
                    corTra = 'beige';
                    break;
                default:
                    corTra = 'white';
                    break;
            }

            let div = document.createElement('div');
            div.className = 'tinta';
            div.style.backgroundColor = corTra;
            //inserindo filtro
            div.addEventListener('click', () => {
                this.mostraProdutos(listaProds, cor, 'color')
            });
            //inserindo filtro
            this.paleta.appendChild(div);
        })
    }

    mostraGeneros(listaProds){
        this.ul.innerHTML = "";
        ["Masculina", "Feminina"].forEach(gen => {
            let li = document.createElement('li');
            let tex = document.createTextNode(gen);
            li.appendChild(tex);
            //inserindo filtro
            li.addEventListener('click', () => {
                this.mostraProdutos(listaProds, gen, "gender");
            });
            //inserindo filtro
            this.ul.appendChild(li);
        });
    }

    mostraProdutos(lista, filtro=false, tipo=null){
        
        //limpa mostruario
        this.corpo.innerHTML = "";

        let listaFiltrada;
        if(filtro){
            listaFiltrada = lista.items.filter((elem) => {
                return elem.filter[0][tipo] == filtro;
            });
        } else {
            listaFiltrada = lista.items;
        }

        listaFiltrada.forEach(item => {
            let card = document.createElement('card');
            card.className = "card";

            let img = document.createElement('img');
            img.src = item.image;

            let nome = document.createElement('h3');
            let textNome = document.createTextNode(item.name);
            nome.appendChild(textNome);

            let preco = document.createElement('h3');
            let textPreco = document.createTextNode("R$ "+item.price);
            preco.appendChild(textPreco);

            let btn = document.createElement("div");
            let textBtn = document.createTextNode("Comprar");
            btn.className = "btnComprar";
            btn.appendChild(textBtn);

            [img, nome, preco, btn].forEach(ele => card.appendChild(ele));

            this.corpo.appendChild(card);
        })
    }
}