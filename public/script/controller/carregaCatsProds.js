
//script para carregar as categorias
class CarregaCatsProds{

    constructor(){
        //Carrega view para mostrar as categorias
        this.mostraCats = new MostraCategorias();
        
        this.listaCats = [];

        //Carregando a lista de categorias e envia p/ view
        this.carregaCats();
    }

    carregaCats(){
        //Carrega lista de categorias e envia p/ view mostrar
        avisos.aviso("Carregando categorias");
        new AcessaAPI().
            buscaDados(categoriasAPI).
            then(resp => {
                this.listaCats = JSON.parse(resp).items;
                avisos.esconde();

                //Pede ao obj q ctrl a view p/ mostrar as categorias
                this.mostraCats.mostra(this.listaCats, this); 
            });
    }

    carregaProds(lista, id){
        //CRIAR CLASSE DESTE METODO
        avisos.aviso("Carregando produtos");

        //Carrega view para mostrar os produtos
        let mostraProds = new MostraProdutos();

        let catAtual;
        lista.forEach(cat => {
            if(cat.id == id) {
                catAtual = cat;
            }
        })

        mostraProds.mostraTitulos(catAtual.name);

        new AcessaAPI().
            buscaDados(produtosAPI+id).
            then(resp =>{
                let listaProds = JSON.parse(resp)
                mostraProds.filtros(listaProds);
                mostraProds.mostraProdutos(listaProds) //Cara que mostra os produtos; AQUI EM Q SER INSERIDO OS FILTROS
                avisos.esconde();
            })
    }

}